from email import message
from logging import WARNING
from pyexpat.errors import messages
from sre_parse import SPECIAL_CHARS
from django.shortcuts import render
from django.contrib import messages

# Create your views here.
def index(request):
    special_chars=["%","#","@","!","$","^","*",";",",",".","<",">",""]
    if request.method=="POST":
        prod=request.POST["name"]
#        if '@' in prod or '-' in prod or '|' in prod or ';' in prod or '[' in prod:
 #          messages.info(request,"there is some specal chars in your search filed")
        if len(prod)>80 :
            messages.error(request,"you have entered more than 80 chars")
        elif prod in iter(special_chars):
            messages.error(request,"your input has special chars")
    return render(request,'index.html')